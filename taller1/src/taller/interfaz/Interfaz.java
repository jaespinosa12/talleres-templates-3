package taller.interfaz;

import java.util.ArrayList;
import java.util.Scanner;

import taller.mundo.Jugador;
import taller.mundo.LineaCuatro;

public class Interfaz 
{
	/**
	 * Juego actual
	 */
	private LineaCuatro juego;
	
	/**
	 * Escáner de texto ingresado por el usuario en la consola
	 */
	private Scanner sc;
	
	/**
	 * Crea una nueva instancia de la clase de interacción del usuario con la consola
	 */
	public Interfaz()
	{
		sc= new Scanner (System.in);
		while (true)
		{
			imprimirMenu();
			try
			{
				int opt=Integer.parseInt(sc.next());
				if(opt==1)
				{
					empezarJuego();
				}
				else if(opt==2)
				{
					empezarJuegoMaquina();
				}
				else if(opt==3)
				{
					System.out.println("¡Vuelva pronto!");
					break;
				}
				else
				{
					System.out.println("Comando inválido");
					continue;
				}
			}
			catch (NumberFormatException e)
			{
				System.out.println("Comando inválido");
				continue;
			}


		}
	}
	/**
	 * Imprime el menú principal
	 */
	public void imprimirMenu()
	{
		System.out.println("------------------------LÍNEA CUATRO------------------------");
		System.out.println("-Menú principal-");
		System.out.println("Ingrese un comando:");
		System.out.println("1. Empezar juego con amigos");
		System.out.println("2. Empezar juego vs máquina");
		System.out.println("3. Salir");		
    }
	
	/**
	 * Crea un nuevo juego
	 */
	public void empezarJuego()
	{
       //TODO
		ArrayList<Jugador> play = new ArrayList<Jugador>();
		Scanner temp = new Scanner (System.in);
		String mas = "si";
		while( mas.equals("si"))
		{
			System.out.println("Elija el nombre del jugador");
			String name = temp.nextLine();
			System.out.println("Elija el símbolo del jugador");
			String sym = temp.nextLine();
			Jugador x = new Jugador(name, sym);
			play.add(x);
			System.out.println("Hay algún otro jugador? Escriba si o no");
			mas = temp.nextLine();
		}
		System.out.println("Ingrese el número de filas");
		int row = temp.nextInt();
		System.out.println("Ingrese el número de columnas");
		int colu = temp.nextInt();
		juego = new LineaCuatro(play, row, colu);
		imprimirTablero();
	}
	
	/**
	 * Modera el juego entre jugadores
	 */
	public void juego()
	{
		//TODO
	}
	/**
	 * Empieza el juego contra la máquina
	 */
	public void empezarJuegoMaquina()
	{
		//TODO
		ArrayList<Jugador> play = new ArrayList<Jugador>();
		Scanner temp = new Scanner (System.in);
		System.out.println("Escriba su nombre");
		String name = temp.nextLine();
		System.out.println("Elija un símbolo");
		String sym = temp.nextLine();
		Jugador x = new Jugador(name, sym);
		System.out.println("Elija el número de filas");
		int fil = temp.nextInt();
		System.out.println("Elija el número de columnas");
		int col = temp.nextInt();
		play.add(x);
		juego = new LineaCuatro(play, fil, col);
		imprimirTablero();
	}
	/**
	 * Modera el juego contra la máquina
	 */
	public void juegoMaquina()
	{
		//TODO
	}

	/**
	 * Imprime el estado actual del juego
	 */
	public void imprimirTablero()
	{
		//TODO
		for( int i = 0; i < juego.darTablero().length; i++)
		{
			for(int j=0;j<juego.darTablero()[0].length;j++)
			{
				System.out.println(juego.darTablero()[i][j]);
			}
		}
	}
}
